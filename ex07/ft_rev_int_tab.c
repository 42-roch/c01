/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/03 10:36:41 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/03 10:59:04 by rblondia         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>

void	ft_rev_int_tab(int *tab, int size)
{
	int	i;
	int	a;
	int	tmp;

	a = 0;
	i = size - 1;
	while (a < (size / 2))
	{
		tmp = tab[a];
		tab[a] = tab[i];
		tab[i] = tmp;
		a++;
		i--;
	}
}

/*
** int main()
** {
** 	int size = 10;
** 	int tab[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
** 	int *tab1;
** 
** 	tab1 = tab;
** 	ft_rev_int_tab(tab1, size);
** 
** 	int loop;
** 
**     	for(loop = 0; loop < 10; loop++)
**        printf("%d ", tab[loop]);
** }
*/