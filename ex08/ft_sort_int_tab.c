/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/03 11:58:11 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/03 13:32:48 by rblondia         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_int_tab(int *tab, int size)
{
	int	i;
	int	a;
	int	tmp;

	a = 0;
	while (a < (size - 1))
	{
		i = a + 1;
		if (tab[a] > tab[i])
		{
			tmp = tab[a];
			tab[a] = tab[i];
			tab[i] = tmp;
			a = 0;
		}
		a++;
	}
}
/*
** int main()
** {
**     int tab[] = {10, 25, 75, 105, 50, 1050, 35, 78, 100};
**     int* tab1;
** 
**     tab1 = tab;
**     ft_sort_int_tab(tab1, 9);
** 
**     int loop;
**     for (loop = 0; loop < 9; loop++)
**         printf("%d - ", tab[loop]);
** }
** 
*/
